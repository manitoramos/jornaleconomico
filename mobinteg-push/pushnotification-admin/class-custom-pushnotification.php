<?php
ini_set('max_execution_time', 600); //600 seconds = 10 minutes
include_once plugin_dir_path(__FILE__).'/../php-pusher/vendor/autoload.php';

use Mobinteg\Pusher\Device;
use Mobinteg\Pusher\Payload;
use Mobinteg\Pusher\Options;
use Mobinteg\Pusher\Pusher;


function custom_PushNotifications_wp_html(){
?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
jQuery(document).ready(function() {
		//initialize the pqSelect widget.
		// jQuery("#selected_user").pqSelect({
		// 		multiplePlaceholder: 'Select User',
		// 		checkbox: true //adds checkbox to options
		// }).on("change", function(evt) {
		// 	var val = jQuery(this).val();
		// }).pqSelect('close');

		//THis is for selecting the post
		$(document).ready(function() {
		    $('.selected_post').select2();
		});

		// validate signup form on keyup and submit
		jQuery("#custom_pushnotification_form").validate({
				ignore:'',
				rules: {
					'selected_user[]': {
					 //required: true
				},
				message_text: {
				required: false,
				maxlength: 235
				},
				msg_title: "required",
				only_ios: {
				required: function() {
						if(jQuery("#only_android").prop('checked')) {
							return false;
						}
						return true;
					}
				},
				only_android: {
				required: function() {
						if(jQuery("#only_ios").prop('checked')) {
							return false;
						}
						return true;
					}
				}
				},
			messages: {
				'selected_user[]': "Please Select Users",
				msg_title: "Please enter your Message title",
				message_text: {
					required: "Please enter a Message",
					minlength: "Your Message Must not be more than 235 characters"
				}
			},
			errorPlacement: function(error, element) {
				jQuery(element).closest('tr').next().find('.error_label').html(error);
			}
		});

	});
</script>
<form name="custom_pushnotification_form" action="" id="custom_pushnotification_form" method="post">
<table>
	<tr>
		<td>
		<h2><?php _e('Send Your Message via Push Notifications')?></h2>
		</td>
	</tr>
	<p> </p>
	<tr>
		<td cols="2">
		<h2><?php _e('What is Custom Push Notifications?');?> </h2>
		<p> <?php _e('- With this feature you can Send Your custom message via push notification to all or any specific WordPress users. It is importnat when you want to pass specific information to only limited user as per your requirement');?><p>

		<h2><?php _e('When to use Custom Push Notifications?');?></h2>
		<p> <?php _e('- When you want to Send push notification to WordPress users selectively. ');?><p>
		<p> <?php _e('- When you want to Send any custom message to any user directly from here. ');?><p>
		<p> <?php _e('- When you want to Send custom message to only ios mobile device user or only to android mobile users. ');?><p>

	    <h2><?php _e('How to send any custom message to any wp user?');?></h2>
		<p> <?php _e('- Just select the user login name from select box type your message title and text. select desired users ios / android device for notification. ');?><p>


		</td>
	</tr>
	<p> </p>
</table>

<table>
<?php
	global  $wp , $wpdb;
	$blogtime = current_time( 'mysql' );
	$current_date = date("Y-m-d H:i:s");

	//echo "mi00";

  //require_once plugin_dir_path( dirname( __FILE__ ) ) /. '/pushnotification/class-pushnotification.php';

	$current_url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$redirect_url='?page=all-Pushnotifications-wp';

	wp_nonce_field( 'custom_message', 'apn_custom_message' );

	$all_pushnotification_users = "{$wpdb->prefix}all_pushnotification_token";

	$all_users = $wpdb->get_results("SELECT
																	 user.user_nicename AS nicename,
																	 user.ID AS ID
																	 FROM {$wpdb->prefix}all_pushnotification_token AS tokens
																	 INNER JOIN {$wpdb->prefix}users AS user ON user.ID = tokens.user_id LIMIT 100");

	$all_posts = $wpdb->get_results("SELECT ID, POST_TITLE, POST_TYPE, GUID
																		FROM {$wpdb->prefix}posts
																		WHERE POST_TYPE in ('post') AND post_status = 'publish'
																		ORDER BY POST_DATE DESC
																		LIMIT 1000");

	$error = false;
	$error_ios = false;
	$error_android = false;
	$old_gcm_mode = false;
	//$apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/'.get_option('ios_certi_name');
	$apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/pushcert.pem';
	$apns_certificate_password = 'mobinteg123!';
	$apns_production = get_option('send_via_production') == "yes";
	$allpush_google_api_key= get_option('allpush_google_api_key');

	define('OLD_GCM_MODE', $old_gcm_mode);
	define('APNS_CERTIFICATE_PATH', $apns_certificate_path);
	define('APNS_CERTIFICATE_PASSWORD', $apns_certificate_password);
	define('APNS_PRODUCTION', $apns_production);
	define('GOOGLE_API_KEY', $allpush_google_api_key);

	if(isset($_POST['send_now_button'])){

			$users_ids = array_column($all_users, 'ID');

			if(isset($_POST['only_ios'])) { $only_ios = $_POST['only_ios']; } else {$only_ios = '';}
			if(isset($_POST['only_android'])) { $only_android = $_POST['only_android']; } else {$only_android = ''; }

			$all_pushnotification_token = $wpdb->prefix . 'all_pushnotification_token';
			$all_device_tokens = $wpdb->get_results("SELECT device_token FROM $all_pushnotification_token");

			if(!wp_verify_nonce($_POST['apn_custom_message'], 'custom_message' )) {

					 print 'Sorry, your nonce did not verify.';
					//exit;

			}
			else{
					//echo "mi01";
					if( (!empty($users_ids)) && (!empty($all_device_tokens)) ){

							$all_userDevices = array();

							$message = array("message" => $_POST['message_text'],"title" => $_POST['msg_title']);
							$messageTitle = array("title" => $_POST['msg_title']);
							$messageBody = array("message" => $_POST['message_text']);

							$select_post = array("selected_post" => $_POST['selected_post']);

							$all_pushnotification_token = $wpdb->prefix . "all_pushnotification_token";

							foreach($users_ids as $selected_user_id)
							{
								$user_data=$wpdb->get_row("SELECT device_token,os_type FROM `$all_pushnotification_token` where user_id=".$selected_user_id);

								if(!empty($user_data->os_type)) { $deviceType = $user_data->os_type; } else {  $deviceType = ''; }
								if(!empty($user_data->device_token)){ $deviceToken = $user_data->device_token; } else { $deviceToken = '';}

								if ($deviceType == 'android' && $only_android !='') {
									array_push($all_userDevices, new Device($deviceToken, 'android'));
								} elseif ($deviceType == 'ios' && $only_ios !='') {
									array_push($all_userDevices, new Device($deviceToken, 'ios'));
								}

							}

							if(!empty($only_ios)) {

								$ios_certi_name=get_option('ios_certi_name');
								/*if(empty($ios_certi_name) || strlen($ios_certi_name) <= 0) {
									$error_ios = true;
								}*///SOME ERROR ON UPLOAD
							}

							if(!empty($only_android)) {
								if(empty($allpush_google_api_key) || strlen($allpush_google_api_key) <= 0) {
									$error_android = true;
								}
							}

							if($error_ios == false && $error_android == false) {

								$options = new Options( GOOGLE_API_KEY, APNS_CERTIFICATE_PATH, APNS_CERTIFICATE_PASSWORD, APNS_PRODUCTION, OLD_GCM_MODE );
								$pusher = new Pusher( $options );

								// Formating notifications sent
								$message_title = array_values($messageTitle)[0];
								$message_body = array_values($messageBody)[0];
								$selected_post = array_values($select_post)[0];
								$get_selected_post_id = explode('__', $selected_post, 2);
								$content_id = $get_selected_post_id[0];
								$selected_event_type = explode('__', $get_selected_post_id[1], 2);
								$message_type = $selected_event_type[0];

								$content_url = get_site_url() . "/wp-json/je/v1/feed?details=" . $content_id;

								$payload = new Payload( $message_title, $message_body, $message_type, $content_id, $content_url );

								$pusher->send($all_userDevices, $payload);

								$all_pushnotification_logs = $wpdb->prefix . 'all_pushnotification_logs';
								$all_pushnotifications = $wpdb->prefix . 'all_pushnotifications';

								//getting a list of the selected users tokens
								if(!empty($all_userDevices)) {
									foreach($all_userDevices as &$value) {
											$users_token_list[] = $value->token;
									}

									//insert logic for every user selected
									foreach ( $users_token_list as $registation_ids ) {
										//insert into all_pushnotification_logs
										$wpdb->insert($all_pushnotification_logs,array('push_title' => $message_title,'push_message' => $message_body,'push_sent' => 1,'push_send_date' => $blogtime,'devicetoken_id' =>$registation_ids),array('%s','%s','%d','%s','%s'));

										//getting the post log_id
										$log_id_arr = $wpdb->get_results("SELECT log_id FROM $all_pushnotification_logs ORDER BY log_id DESC LIMIT 1");
										$log_id_obj = array_values($log_id_arr)[0];
										$log_id = print_r($log_id_obj->log_id, 1);

										//get the users_ids
										$user_id_result = $wpdb->get_results("SELECT user_id FROM $all_pushnotification_token where device_token='{$registation_ids}'");
										$user_id = $user_id_result[0]->user_id;

										//insert into $all_pushnotifications
										$wpdb->insert($all_pushnotifications,array('log_id' => $log_id, 'user_id' => $user_id, 'push_title' => $message_title, 'push_message' => $message_body, 'devicetoken_id' => $registation_ids, 'type' => $message_type, 'post_id' => $content_id, 'content_url' => $content_url , 'date' => $current_date),array('%d','%d','%s','%s','%s','%s','%d','%s','%s'));
									}
								}

								//echo "mi..debug";
								$my_file = '_mylog.txt';
								$handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file);
								$data = 'mi..debug'."\n";
								fwrite($handle, $data);
							}
						}
						else{
							$message = __( 'There was an error sending push notification message, There is no device tokens in table.' );
							printf("<p class='error'>%s</p>", $message);
						}
			}

			if($error_ios == true && $error_android == false)  {?>
			<tr>
				<td colspan="2">
				<?php
					$message = __( 'There was an error sending push notification message to ios mobile devices, please check Settings page and verify PEM certificate file for APNs.' );
					printf("<p class='error'>%s</p>", $message);
				?>
				</td>
			</tr>
			<?php } else if( $error_android == true && $error_ios == false ) {?>
			<tr>
				<td colspan="2">
				<?php
					$message = __( 'There was an error sending push notification message to android mobile devices, please check Settings page and verify GCM / FCM Key.' );
					printf("<p class='error'>%s</p>", $message);
				?>
				</td>
			</tr>
			<?php } else if( $error_android == true && $error_ios == true ) { ?>
			<tr>
				<td colspan="2">
				<?php
					$message = __( 'There was an error sending push notification message to mobile devices, please check Settings page and verify PEM certificate file for APNs and also verify GCM / FCM Key.' );
					printf("<p class='error'>%s</p>", $message);
				?>
				</td>
			</tr>
		<?php
			}
			else { ?>
		<tr>
			<td colspan="2">
			<?php
				$message = __( 'Message sent successfully. Please check the push notification in device.' );
				printf("<p class='suceess' style='color:green'>%s</p>", $message);
			?>
			</td>
		</tr>
		<?php
				}

		} /*post end*/

		?>

	<tr>
		<td><?php _e('Message Title')?>: </td>
		<td><input type="text" name="msg_title" value="Hi there!" required></td>
	</tr>
	<tr>
		<td></td>
		<td><span class="error_label"></span></td>
	</tr>

	<tr>
		<td><?php _e('Message Text')?>: </td>
		<td><textarea rows="5" cols="25" name="message_text" value="How are you today?"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td><span class="error_label"></span></td>
	</tr>

	<tr>
		<td><?php _e('Select Post')?>:</td>
		<td>
			<label for="id_label_single">
				<select id="selected_post" name="selected_post" class="selected_post js-states form-control" style="margin: 20px;width:300px;">
				<?php
				foreach ( $all_posts as $posts )
				{
					echo '<option value='.$posts->ID."__".$posts->POST_TYPE."__".$posts->GUID.'>'.$posts->POST_TITLE.'</option>';
				}
				?>

				</select>
			</label>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><span class="error_label"></span></td>
	</tr>


	<tr>
		<td><?php _e('Send Push Notifications To')?>:</td>
		<td>
		<input type="checkbox" name="only_ios" value="yes" id="only_ios" class="checkBox_class">
		<label for="only_ios"><?php _e('iOS devices')?></label>
		<br>
		<input type="checkbox" name="only_android" value="yes" id="only_android" class="checkBox_class">
		<label for="only_android"><?php _e('Android devices')?></label><br>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><span class="error_label"></span></td>
	</tr>


	<tr>
		<td></td>
		<td><span><?php _e('Note: Please make sure you have done valid setting under "All Settings" menu.')?></span></td>
	</tr>
	<tr>
		<td>
		<input type="submit" value="<?php _e('Send Now')?>" name="send_now_button" id="send_now_button" class="button button-primary">
		</td>
	</tr>
	</table>
	<br>
	<br>
</form>
<?php
}


function post_published_notification( $post_id ) {

	global $wpdb;
	$send = get_post_meta( $post_id, "send_push_notification", true );

	$res = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}posts WHERE ID = {$post_id}" );
	if( $res->post_status != 'publish' ) return;

	if ($send == "sim"){
		//change to default stage
		update_post_meta( $post_id, "send_push_notification", "nao" );

	include_once plugin_dir_path(__FILE__).'/../php-pusher/vendor/autoload.php';
	include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Device.php';
	include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Payload.php';
	include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Options.php';
	include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Pusher.php';


	global  $wp , $wpdb;
	$blogtime = current_time( 'mysql' );
	$current_date = date("Y-m-d H:i:s");

	wp_nonce_field( 'custom_message', 'apn_custom_message' );

	$all_users = $wpdb->get_results("SELECT
																	 user.user_nicename AS nicename,
																	 user.ID AS ID,
																	 tokens.os_type AS os_type,
																	 tokens.device_token AS device_token
																	 FROM {$wpdb->prefix}all_pushnotification_token AS tokens
																	 INNER JOIN {$wpdb->prefix}users AS user ON user.ID = tokens.user_id
																	 ORDER BY device_token");

	$old_gcm_mode = false;
	//$apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/'.get_option('ios_certi_name');
    $apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/pushcert.pem';
	$apns_certificate_password = 'mobinteg123!';
	$apns_production = get_option('send_via_production') == "yes";
	$allpush_google_api_key= get_option('allpush_google_api_key');

	define('OLD_GCM_MODE', $old_gcm_mode);
	define('APNS_CERTIFICATE_PATH', $apns_certificate_path);
	define('APNS_CERTIFICATE_PASSWORD', $apns_certificate_password);
	define('APNS_PRODUCTION', $apns_production);

	$users_ids = array_column($all_users, 'ID');

	if(isset($_POST['only_ios'])) { $only_ios = $_POST['only_ios']; } else {$only_ios = '';}

		$all_userDevices = array();

		$all_pushnotification_token = $wpdb->prefix . "all_pushnotification_token";

		foreach($all_users as $user)
		{
			//$user_data=$wpdb->get_row("SELECT device_token,os_type FROM `$all_pushnotification_token` where user_id=".$selected_user_id);

			if(!empty($user->os_type)) { $deviceType = $user->os_type; } else {  $deviceType = ''; }
			if(!empty($user->device_token)){ $deviceToken = $user->device_token; } else { $deviceToken = '';}

			if ($deviceType == 'android') {
				array_push($all_userDevices, new Device($deviceToken, 'android'));
			} elseif ($deviceType == 'ios') {
				array_push($all_userDevices, new Device($deviceToken, 'ios'));
			}
		}

			$ios_certi_name=get_option('ios_certi_name');
			if(empty($ios_certi_name) || strlen($ios_certi_name) <= 0) {
				$error_ios = true;
			}

			if(empty($allpush_google_api_key) || strlen($allpush_google_api_key) <= 0) {
				$error_android = true;
			}
			if (defined('GOOGLE_API_KEY')) {
				//echo "-> google api set\n";
				//exit();
			} else {
				define('GOOGLE_API_KEY', $allpush_google_api_key);
			}
			$options = new Options( GOOGLE_API_KEY, APNS_CERTIFICATE_PATH, APNS_CERTIFICATE_PASSWORD, APNS_PRODUCTION, OLD_GCM_MODE );
			$pusher = new Pusher( $options );

			// Formating notifications sent
			$message_title = '';
			$message_body = html_entity_decode(get_the_title($post_id));
			$message_type = "post";
			$content_id = (string)get_the_id($post_id);

			$content_url = get_site_url() . "/wp-json/je/v1/feed?details=" . $content_id;

			$payload = new Payload( $message_title, $message_body, $message_type, $content_id, $content_url );

			$pusher->send($all_userDevices, $payload);

			$all_pushnotification_logs = $wpdb->prefix . 'all_pushnotification_logs';
			$all_pushnotifications = $wpdb->prefix . 'all_pushnotifications';

			//getting a list of the selected users tokens
			if(!empty($all_userDevices)) {
				foreach($all_userDevices as &$value) {
						$users_token_list[] = $value->token;
				}

				//insert logic for every user selected
				foreach ( $users_token_list as $registation_ids ) {
					//insert into all_pushnotification_logs
					$wpdb->insert($all_pushnotification_logs,array('push_title' => $message_title,'push_message' => $message_body,'push_sent' => 1,'push_send_date' => $blogtime,'devicetoken_id' =>$registation_ids),array('%s','%s','%d','%s','%s'));

					//getting the post log_id
					$log_id_arr = $wpdb->get_results("SELECT log_id FROM $all_pushnotification_logs ORDER BY log_id DESC LIMIT 1");
					$log_id_obj = array_values($log_id_arr)[0];
					$log_id = print_r($log_id_obj->log_id, 1);

					//get the users_ids
					$user_id_result = $wpdb->get_results("SELECT user_id FROM $all_pushnotification_token where device_token='{$registation_ids}'");
					$user_id = $user_id_result[0]->user_id;

					//insert into $all_pushnotifications
					$wpdb->insert($all_pushnotifications,array('log_id' => $log_id, 'user_id' => $user_id, 'push_title' => $message_title, 'push_message' => $message_body, 'devicetoken_id' => $registation_ids, 'type' => $message_type, 'post_id' => $content_id, 'content_url' => $content_url , 'date' => $current_date),array('%d','%d','%s','%s','%s','%s','%d','%s','%s'));
				}
			}
		}
}
add_action('acf/save_post', 'post_published_notification', 20 );

?>

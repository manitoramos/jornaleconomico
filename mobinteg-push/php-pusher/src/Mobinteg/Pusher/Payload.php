<?php


namespace Mobinteg\Pusher;


class Payload {

  /**
   * @param $title string
   * @param null $body string
   * @param null $type string
   * @param null $data_id int
   * @param null $data array
   * @param null $badge int
   * @param null $sound string
   * @param null $expiry int
   */
  public function __construct ( $title, $body, $type, $data_id, $data, $badge = "default", $sound = null, $expiry = null) {
    $this->title = $title;
    $this->body = $body;
    $this->type = $type;
    $this->data_id = $data_id;
    $this->data = $data;
    $this->badge = $badge;
    $this->sound = $sound;
    $this->expiry = $expiry;
  }
}

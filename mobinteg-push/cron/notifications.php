<?php
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', 1 );
ini_set('max_execution_time', 600); //600 seconds = 10 minutes

use Mobinteg\Pusher\Device;
use Mobinteg\Pusher\Payload;
use Mobinteg\Pusher\Options;
use Mobinteg\Pusher\Pusher;

if ( !defined( 'ABSPATH' ) ) {
    require_once __DIR__ . '/../../../../wp-load.php';
}
include_once plugin_dir_path(__FILE__).'/../php-pusher/vendor/autoload.php';

verifyNewNotifications();
/**
 *
 */
function verifyNewNotifications()
{
    global $wpdb;
    $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}new_notifications WHERE run = 'no' ORDER BY id ASC LIMIT 1");
    allDevices($res[0]->postID,intval($res[0]->runned));
}

function allDevices($post_id,$limit)
{
    global $wpdb;
    //$send = get_post_meta( $post_id, "send_push_notification", true );

    $limit = $limit * 1000;

    $res = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}posts WHERE ID = {$post_id}" );
    if( $res->post_status != 'publish' ){
        $wpdb->query("UPDATE {$wpdb->prefix}new_notifications SET run = 'yes' WHERE postID = {$post_id}");
        return;
    }
    $wpdb->query("UPDATE {$wpdb->prefix}new_notifications SET runned = runned + 1 WHERE postID = {$post_id}");

    //if ($send == "sim"){
        //change to default stage
       //update_post_meta( $post_id, "send_push_notification", "nao" );

        include_once plugin_dir_path(__FILE__).'/../php-pusher/vendor/autoload.php';
        include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Device.php';
        include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Payload.php';
        include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Options.php';
        include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Pusher.php';


        global  $wp , $wpdb;
        $blogtime = current_time( 'mysql' );
        $current_date = date("Y-m-d H:i:s");

        wp_nonce_field( 'custom_message', 'apn_custom_message' );

        $all_users = $wpdb->get_results("SELECT
																	 user.user_nicename AS nicename,
																	 user.ID AS ID,
																	 tokens.os_type AS os_type,
																	 tokens.device_token AS device_token
																	 FROM {$wpdb->prefix}all_pushnotification_token AS tokens
																	 INNER JOIN {$wpdb->prefix}users AS user ON user.ID = tokens.user_id
																	 ORDER BY push_token_id LIMIT {$limit},1000");
        //if have less then 1000 users put it runned
        if(sizeof($all_users) < 1000) $wpdb->query("UPDATE {$wpdb->prefix}new_notifications SET run = 'yes' WHERE postID = {$post_id}");

        $old_gcm_mode = false;
        //$apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/'.get_option('ios_certi_name');
        $apns_certificate_path = plugin_dir_path(__FILE__).'../../../uploads/ioscerti/pushcert.pem';
        $apns_certificate_password = 'mobinteg123!';
        $apns_production = get_option('send_via_production') == "yes";
        $allpush_google_api_key= get_option('allpush_google_api_key');

        define('OLD_GCM_MODE', $old_gcm_mode);
        define('APNS_CERTIFICATE_PATH', $apns_certificate_path);
        define('APNS_CERTIFICATE_PASSWORD', $apns_certificate_password);
        define('APNS_PRODUCTION', $apns_production);

        $users_ids = array_column($all_users, 'ID');

        if(isset($_POST['only_ios'])) { $only_ios = $_POST['only_ios']; } else {$only_ios = '';}

        $all_userDevices = array();

        $all_pushnotification_token = $wpdb->prefix . "all_pushnotification_token";
        //for counting 1000 devices
        $devvi = 0;
        foreach($all_users as $user)
        {
            //$user_data=$wpdb->get_row("SELECT device_token,os_type FROM `$all_pushnotification_token` where user_id=".$selected_user_id);
            if(!empty($user->os_type)) { $deviceType = $user->os_type; } else {  $deviceType = ''; }
            if(!empty($user->device_token)){ $deviceToken = $user->device_token; } else { $deviceToken = '';}

            if ($deviceType == 'android') {
                array_push($all_userDevices, new Device($deviceToken, 'android'));
                $devvi++;
            } elseif ($deviceType == 'ios') {
                array_push($all_userDevices, new Device($deviceToken, 'ios'));
                //$devvi++;
            }
            if($devvi >= 1000){
                sendNotifications($all_userDevices,$post_id);
                $all_userDevices = [];
            }
        }

        $ios_certi_name=get_option('ios_certi_name');
        if(empty($ios_certi_name) || strlen($ios_certi_name) <= 0) {
            $error_ios = true;
        }

        if(empty($allpush_google_api_key) || strlen($allpush_google_api_key) <= 0) {
            $error_android = true;
        }
        if (defined('GOOGLE_API_KEY')) {
            //echo "-> google api set\n";
            //exit();
        } else {
            define('GOOGLE_API_KEY', $allpush_google_api_key);
        }
        $options = new Options( GOOGLE_API_KEY, APNS_CERTIFICATE_PATH, APNS_CERTIFICATE_PASSWORD, APNS_PRODUCTION, OLD_GCM_MODE );
        $pusher = new Pusher( $options );

        // Formating notifications sent
        $message_title = '';
        $message_body = html_entity_decode(get_the_title($post_id));
        $message_type = "post";
        $content_id = (string)$post_id;

        $content_url = get_site_url() . "/wp-json/je/v1/feed?details=" . $content_id;

        $payload = new Payload( $message_title, $message_body, $message_type, $content_id, $content_url );

        $pusher->send($all_userDevices, $payload);

        $all_pushnotification_logs = $wpdb->prefix . 'all_pushnotification_logs';
        $all_pushnotifications = $wpdb->prefix . 'all_pushnotifications';

        //getting a list of the selected users tokens
        if(!empty($all_userDevices)) {
            foreach($all_userDevices as &$value) {
                $users_token_list[] = $value->token;
            }

            //insert logic for every user selected
            foreach ( $users_token_list as $registation_ids ) {
                //insert into all_pushnotification_logs
                $wpdb->insert($all_pushnotification_logs,array('push_title' => $message_title,'push_message' => $message_body,'push_sent' => 1,'push_send_date' => $blogtime,'devicetoken_id' =>$registation_ids),array('%s','%s','%d','%s','%s'));

                //getting the post log_id
                $log_id_arr = $wpdb->get_results("SELECT log_id FROM $all_pushnotification_logs ORDER BY log_id DESC LIMIT 1");
                $log_id_obj = array_values($log_id_arr)[0];
                $log_id = print_r($log_id_obj->log_id, 1);

                //get the users_ids
                $user_id_result = $wpdb->get_results("SELECT user_id FROM $all_pushnotification_token where device_token='{$registation_ids}'");
                $user_id = $user_id_result[0]->user_id;

                //insert into $all_pushnotifications
                $wpdb->insert($all_pushnotifications,array('log_id' => $log_id, 'user_id' => $user_id, 'push_title' => $message_title, 'push_message' => $message_body, 'devicetoken_id' => $registation_ids, 'type' => $message_type, 'post_id' => $content_id, 'content_url' => $content_url , 'date' => $current_date),array('%d','%d','%s','%s','%s','%s','%d','%s','%s'));
            }
        }
    //}
}



/**
 * When have more then 1000 devices
 * @param $all_userDevices
 * @param $post_id
 */
function sendNotifications($all_userDevices,$post_id)
{
    $allpush_google_api_key= get_option('allpush_google_api_key');
    include_once plugin_dir_path(__FILE__).'/../php-pusher/vendor/autoload.php';
    include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Device.php';
    include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Payload.php';
    include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Options.php';
    include_once plugin_dir_path(__FILE__).'/../php-pusher/src/Mobinteg/Pusher/Pusher.php';
    global $wpdb;
    $blogtime = current_time( 'mysql' );
    $current_date = date("Y-m-d H:i:s");
    $all_pushnotification_token = $wpdb->prefix . "all_pushnotification_token";
    $ios_certi_name=get_option('ios_certi_name');
    if(empty($ios_certi_name) || strlen($ios_certi_name) <= 0) {
        $error_ios = true;
    }

    if(empty($allpush_google_api_key) || strlen($allpush_google_api_key) <= 0) {
        $error_android = true;
    }
    if (defined('GOOGLE_API_KEY')) {
        //echo "-> google api set\n";
        //exit();
    } else {
        define('GOOGLE_API_KEY', $allpush_google_api_key);
    }
    $options = new Options( GOOGLE_API_KEY, APNS_CERTIFICATE_PATH, APNS_CERTIFICATE_PASSWORD, APNS_PRODUCTION, OLD_GCM_MODE );
    $pusher = new Pusher( $options );

    // Formating notifications sent
    $message_title = '';
    $message_body = html_entity_decode(get_the_title($post_id));
    $message_type = "post";
    $content_id = (string)get_the_id($post_id);

    $content_url = get_site_url() . "/wp-json/je/v1/feed?details=" . $content_id;

    $payload = new Payload( $message_title, $message_body, $message_type, $content_id, $content_url );

    $pusher->send($all_userDevices, $payload);

    $all_pushnotification_logs = $wpdb->prefix . 'all_pushnotification_logs';
    $all_pushnotifications = $wpdb->prefix . 'all_pushnotifications';

    //getting a list of the selected users tokens
    if(!empty($all_userDevices)) {
        foreach($all_userDevices as &$value) {
            $users_token_list[] = $value->token;
        }

        //insert logic for every user selected
        foreach ( $users_token_list as $registation_ids ) {
            //insert into all_pushnotification_logs
            $wpdb->insert($all_pushnotification_logs,array('push_title' => $message_title,'push_message' => $message_body,'push_sent' => 1,'push_send_date' => $blogtime,'devicetoken_id' =>$registation_ids),array('%s','%s','%d','%s','%s'));

            //getting the post log_id
            $log_id_arr = $wpdb->get_results("SELECT log_id FROM $all_pushnotification_logs ORDER BY log_id DESC LIMIT 1");
            $log_id_obj = array_values($log_id_arr)[0];
            $log_id = print_r($log_id_obj->log_id, 1);

            //get the users_ids
            $user_id_result = $wpdb->get_results("SELECT user_id FROM $all_pushnotification_token where device_token='{$registation_ids}'");
            $user_id = $user_id_result[0]->user_id;

            //insert into $all_pushnotifications
            $wpdb->insert($all_pushnotifications,
                array('log_id' => $log_id, 'user_id' => $user_id, 'push_title' => $message_title, 'push_message' => $message_body, 'devicetoken_id' => $registation_ids, 'type' => $message_type, 'post_id' => $content_id, 'content_url' => $content_url , 'date' => $current_date),
                array('%d','%d','%s','%s','%s','%s','%d','%s','%s'));
        }
    }
}

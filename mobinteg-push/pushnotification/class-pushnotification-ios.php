<?php
class PushNotificationsIos {

	public static function sendToIOS($devices, $message) {
		require_once 'ApnsPHP/Autoload.php';

		ini_set('max_execution_time', 600); //600 seconds = 10 minutes
		ini_set("memory_limit","512M");
		set_time_limit(0);

		$upload_dir = wp_upload_dir();
		$ios_certi_name=get_option('ios_certi_name');
		$user_iosdir = '/ioscerti/';
		$ios_certificate_custom_path=$upload_dir['basedir'].$user_iosdir.$ios_certi_name; /*for custom dir url*/
		$error = false;

		//post Option
		$msg_title= $message['title'];
		$message_text= $message['message'];

		if(empty($ios_certi_name) || strlen($ios_certi_name) <= 0) {
            $error = true;
            return $error;
		}


		$push = new ApnsPHP_Push(
			ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
			$ios_certificate_custom_path
		);
		$push->setRootCertificationAuthority($upload_dir['basedir'].$user_iosdir.'entrust_root_certification_authority.pem');
		//$push->setRootCertificationAuthority($upload_dir['basedir'].$user_iosdir.'pushcert.pem');
		ob_start();

		echo "APN - Start";


		$push->connect();

		foreach ($devices as $device) {
			$message = new ApnsPHP_Message($device);
			$message->setText($message_text);
			$message->setBadge(0);
			$push->add($message);

			//Insert msgs into pushnotification log table
			global $wpdb;
			$blogtime = current_time( 'mysql' );
			$all_pushnotification_logs = $wpdb->prefix . 'all_pushnotification_logs';
			$wpdb->insert($all_pushnotification_logs,array('push_title' => $msg_title,'push_message' => $message_text,'push_sent' => 1,'push_send_date' => $blogtime,'devicetoken_id' =>$device),array('%s','%s','%d','%s','%s'));

		}

		$push->send();
		$push->disconnect();


		$aErrorQueue = $push->getErrors();

		echo "mi-debug";
		/*$sandbox = get_option('send_via_sandbox');
		if (!empty($aErrorQueue) && isset($sandbox) && $sandbox != "" && $sandbox != "yes" ) {

			foreach ($aErrorQueue as $aError){
				$invalidDeviceToken = $aError['MESSAGE']->getRecipients()[0];
				$wpdb->query(
					"DELETE  FROM ".$wpdb->prefix ."all_pushnotification_token where device_token = '".$invalidDeviceToken."'"
			  	);

			}
		}*/

		ob_get_clean();


        return $error;

	}
}
?>

<?php
/**
 * Plugin Name: mobinteg
 * Description: WP plugin developed by mobinteg.com
 * Author: Tiago Ramos
 * Author URI: https://mobinteg.com
 * Version: 1.0.0
 */
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', 1 );

//require_once 'services/options_page.php';

add_action( 'rest_api_init', function () {
    register_rest_route( 'je/v1', '/feed', array(
        'methods' => 'GET',
        'callback' => 'feed',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'je/v1', '/transactions', array(
        'methods' => 'POST',
        'callback' => 'transactions',
    ) );
} );

function feed (WP_REST_Request $request)
{

    require_once 'routes/routes.php';
    $routes = new Routes();

    return $routes->CallRoute();

}

function transactions (WP_REST_Request $request)
{

    require_once 'services/Transactions.php';
    $transactions = new Transactions();

    return $transactions->Insert();

}

function cache_home_feed ()
{
    if ( !defined( 'ABSPATH' ) ) {
        require_once __DIR__ . '/../../../wp-load.php';
    }
    global $wpdb;
    //global funcs
    require_once __DIR__ . '/tools/GlobalFuncs.php';

    require_once __DIR__ . '/services/Category.php';
    require_once __DIR__ . '/services/MenuApp.php';

    $menu = new MenuApp();
    $feed = $menu->MenuService();
    $feed = json_encode( $feed );

    $myfile = fopen( __DIR__ . "/cache/home.txt", "w" ) or die( "Unable to open file!" );
    fwrite( $myfile, $feed );
    fclose( $myfile );

    $feed = addslashes( $feed );
    $date = date( "Y-m-d H:i:s" );
    //$wpdb->query( "INSERT INTO {$wpdb->prefix}cron_logs (log,date_run) VALUES ('{$feed}','{$date}')" );
    $wpdb->query( "INSERT INTO {$wpdb->prefix}cron_logs (date_run) VALUES ('{$date}')" );
}

add_action( 'home_feed_cache', 'cache_home_feed' );

/*
 * SCRIPTS
 */

function mytheme_enqueue_front_page_scripts ()
{
    if ( isset( $_COOKIE['mobile'] ) ) {
        if ( is_front_page() ) {
            //wp_enqueue_script( 'homestuff', __DIR__ , array( 'jquery' ), null, true );
            //wp_enqueue_script( 'jquerycolor', get_theme_part( THEME_JS . '/jquery.color.js' ), array( 'jquery' ), null, true );
            wp_enqueue_style( 'hideheader', '/wp-content/plugins/mobinteg/assets/css/hideheaders.css' );
            //wp_enqueue_script( 'mobilejs', '/wp-content/plugins/mobinteg/assets/js/mobile.js' );
        }
    }
}

add_action( 'get_footer', 'mytheme_enqueue_front_page_scripts' );


function add_meta_tags ()
{
    if ( isset( $_COOKIE['mobile'] ) ) {
        echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
        echo '<meta name="viewport" content="width=device-width, user-scalable=no">';
    }
}

add_action( 'wp_head', 'add_meta_tags' );

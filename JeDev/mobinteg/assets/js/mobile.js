var meta = document.createElement('meta');
meta.setAttribute('name', 'viewport');
meta.setAttribute('initial-scale', '1.0');
meta.setAttribute('maximum-scale', '1.0');
meta.setAttribute('minimum-scale', '1.0');
meta.setAttribute('user-scalable', 'no');
document.getElementsByTagName('head')[0].appendChild(meta);

<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 3/13/19
 * Time: 12:14 AM
 */

Class Routes
{

    const JSONFILE = __DIR__ . "/../cache/home.txt";

    public function CallRoute ()
    {
        //global funcs
        require_once __DIR__ . '/../tools/GlobalFuncs.php';

        if ( isset( $_GET['details'] ) && $_GET['details'] != "" ) {
            return $this->DetailsRoute();
        } else if ( isset( $_GET['prices'] ) && $_GET['prices'] != "" ) {
            return $this->PricesRoute();
        } else if ( isset( $_GET['menu'] ) ) {
            return $this->Menu();
        } else if ( isset( $_GET['search'] ) ) {
            return $this->Search();
        } else if ( isset( $_GET['feed'] ) ) {//cache the home feed
            return $this->HomeRoute();
        } else {
            return $this->CacheJson();
        }
    }

    private function CacheJson ()
    {
        if(isset( $_GET['cat'] ))
            return $this->HomeRoute();

        if(isset( $_GET['home'] )) {
            require_once __DIR__ . '/../services/MenuApp.php';
            $menu = new MenuApp();
            return $menu->MenuService();
        }

        if(!file_exists(self::JSONFILE))
            $this->HomeRoute();

        $myfile = fopen( self::JSONFILE, "r" ) or die( "Unable to open file!" );
        $file = fread( $myfile, filesize( self::JSONFILE ) );
        fclose( $myfile );

        return json_decode($file);
    }

    private function HomeRoute ()
    {
        require_once __DIR__ . '/../services/Category.php';
        $category = new Categories();

        if ( isset( $_GET['cat'] ) ) {
            if ( !is_numeric( $_GET['cat'] ) ) return ["status" => "error", "msg" => "category only can be a number"];

            return $category->RequestCategory( $_GET['cat'] );
        }

        require_once __DIR__ . '/../services/MenuApp.php';
        $menu = new MenuApp();
        $feed = $menu->MenuService();

        $myfile = fopen(self::JSONFILE, "w") or die("Unable to open file!");
        fwrite($myfile, json_encode($feed));
        fclose($myfile);
    }

    private function DetailsRoute ()
    {
        require_once __DIR__ . '/../services/PostDetails.php';
        require_once __DIR__ . '/../services/widgets/Ads.php';
        $details = new PostDetails();

        return $details->Details( $_GET['details'] );
    }

    private function PricesRoute ()
    {
        require_once __DIR__ . '/../services/Prices.php';

        return Prices::AllPrices();
    }

    private function Menu ()
    {
        require_once __DIR__ . '/../services/Menu.php';
        $menu = new Menu();

        return $menu->GetMenu();
    }

    private function Search ()
    {
        require_once __DIR__ . '/../services/SearchPosts.php';
        $search = new SearchPosts();

        return $search->Search( $_GET['search'] );
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/21/19
 * Time: 2:54 PM
 */

class PostDetails extends JeTools
{

    public function Details ($post_id)
    {
        global $wpdb;
        $premiumLink = null;

        $res = $wpdb->get_results(
            $wpdb->prepare( "SELECT
                                        posts.ID AS id,
                                        posts.post_date AS date,
                                        posts.post_content AS content,
                                        posts.post_modified AS modified,
                                        posts.post_excerpt AS excerpt,
                                        posts.guid AS link,
                                        posts.post_title AS title,
                                        video.meta_value AS video
                                        FROM {$wpdb->prefix}posts as posts
                                        LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                                        AND video.meta_key = 'mkdf_post_video_id_meta'
                                        WHERE posts.ID = '%d'", [$post_id] ) );

        if ( sizeof( $res ) > 0 ) {
            $post = $res[0];

            $description = $post->content . Ads::GenerateAd( $post->link);

            $img = ["source_url" => get_the_post_thumbnail_url( $post->id )];
            return [
                [
                    "id" => $post->id,
                    "date" => str_replace( ' ', "T", $post->date ),
                    "modified" => str_replace( ' ', "T", $post->modified ),
                    "description" => $description,
                    "excerpt" => $post->excerpt,
                    "link" => $post->link,
                    "title" => $post->title,
                    "format" => $this->PostFormat( $post->id ),
                    "image" => $img != null ? $img : null,
                    "video" => $post->video != "" ? "http://videos.sapo.pt/{$post->video}" : null,
                    "gallery" => $this->postformat == "gallery" ? $this->ImageGallery( $post->content ) : null,
                    "authors" => $this->Authors( $post->id ),
                    "categories" => $this->PostCategories( $post->id ),
                    "premium" => $this->premium,
                    "premiumLink" => $this->PremiumLink( $post->content ),
                    "related" => $this->PostRelated( $post->id ),
                    "ads" => Ads::GenerateAd( $post->link )
                ]
            ];
        }

        return $this->Error( "Post didnt exist" );

    }

}

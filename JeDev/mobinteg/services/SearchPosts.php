<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 4/4/19
 * Time: 5:03 PM
 */

class SearchPosts extends JeTools
{

    private $limit = 20;

    private $page = 0;

    public function Search( $term )
    {
        global $wpdb;
        $items = [];

        $this->PageLimit();

        $query = "SELECT
                    posts.ID AS id,
                    posts.post_date AS date,
                    posts.post_content AS content,
                    posts.post_modified AS modified,
                    posts.guid AS link,
                    posts.post_title AS title,
                    video.meta_value AS video
                FROM
                    {$wpdb->prefix}posts as posts
                        LEFT JOIN
                    {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                        AND video.meta_key = 'mkdf_post_video_id_meta'
                WHERE
                    (posts.post_title LIKE '%s'
                        OR posts.post_content LIKE '%s')
                        AND posts.post_status = 'publish'
                        AND posts.post_type = 'post'
                ORDER BY date DESC
                LIMIT {$this->page},{$this->limit}";

        $res = $wpdb->get_results( $wpdb->prepare( $query, ["%{$term}%","%{$term}%"] ) );

        foreach ( $res as $post ){
            $img = [ "source_url" => get_the_post_thumbnail_url( $post->id ) ];
            $items[] = [
                "id" => $post->id,
                "date" => str_replace(' ', "T", $post->date ),
                "modified" => str_replace(' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "image" => $img != null ? $img : null,
                "video" => $post->video != "" ? "http://videos.sapo.pt/{$post->video}" : null,
                "gallery" => $this->postformat == "gallery" ? $this->ImageGallery( $post->content ) : null,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "premiumLink" => $this->PremiumLink( $post->content ),
                "detailsLink" => $this->details_link . $post->id
                //"related" => $this->PostRelated( $post->id )
            ];

            //set the premium to false
            $this->premium = false;
        }
        return $items;
    }

    private function PageLimit()
    {

        if(isset($_GET['limit'])){
            $this->limit = $_GET['limit'];
        }

        if( isset( $_GET['page'] ) ){
            $this->page = $this->limit * $_GET['page'];
        }

    }

}

<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 3/13/19
 * Time: 12:39 AM
 */

class Prices
{

    static function AllPrices()
    {
        $prices = [];
        global $wpdb;

        $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE post_name = 'app-settings'" );
        $menu_id = $res[0]->ID;

        $res = $wpdb->get_results( "SELECT
                                              *
                                          FROM
                                              {$wpdb->prefix}postmeta
                                          WHERE
                                              post_id = {$menu_id} AND meta_key LIKE 'product%'" );

        for( $i = 0; $i < sizeof( $res ); $i++ ){

            foreach ( $res as $price ) {
                switch ($price->meta_key) {
                    case "product_{$i}_description":
                        $prices[$i]['description'] = $price->meta_value;
                        break;
                    case "product_{$i}_duration":
                        $prices[$i]['duration'] = $price->meta_value;
                        break;
                    case "product_{$i}_excerpt":
                        $prices[$i]['excerpt'] = $price->meta_value;
                        break;
                    case "product_{$i}_product-id":
                        $prices[$i]['product-id'] = $price->meta_value;
                        break;
                    case "product_{$i}_type":
                        $prices[$i]['type'] = $price->meta_value;
                        break;
                }
            }
        }

        return $prices;
    }

}

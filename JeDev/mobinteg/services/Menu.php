<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 3/14/19
 * Time: 11:38 AM
 */

class Menu extends JeTools
{

    private $menuItems = [];

    public function GetMenu()
    {

        global $wpdb;

        $this->GetMenuID();

        $res = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = {$this->menuid} AND meta_key LIKE 'item%'" );

        for ( $i = 0; $i < $res[0]->meta_value; $i++ ){
            $menuItems[$i]["id"] = $i;
            foreach ( $res as $menu ){
                switch ( $menu->meta_key ){
                    case "item_{$i}_title":
                        $menuItems[$i]['title'] = $menu->meta_value;
                        break;
                    case "item_{$i}_link":
                        $menuItems[$i]['link'] = $menu->meta_value;
                        break;
                    case "item_{$i}_type":
                        $menuItems[$i]['type'] = $menu->meta_value;
                        break;
                }
            }
        }

        return $menuItems;

    }

}

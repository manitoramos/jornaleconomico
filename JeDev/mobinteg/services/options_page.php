<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/22/19
 * Time: 5:58 PM
 */

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page( array(
        'page_title' => 'Application Settings',
        'post_id' => 'application-settings',
    ) );

}

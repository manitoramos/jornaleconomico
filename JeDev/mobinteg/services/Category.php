<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 3/11/19
 * Time: 6:21 PM
 */

class Categories extends JeTools
{

    private $categories = array();

    //default limit
    private $limit = 20;

    //pagination is the page number * limit default (20)
    private $page;

    //current page
    private $current_page;

    //unique id for the widget
    private $unique_id;

    private $menu_id;

    //url for widgets options
    private $widgets = "https://dev.megafin.pt/wp-json/acf/v3/options/application-settings";

    /**
     * Loop into each category
     */
    public function RunCategories( $menu_id )
    {
        $this->menu_id = $menu_id;

        $this->ListCategories();

        $json_data = [];

        for( $i = 0; $i < sizeof( $this->categories ); $i++ ){
            //unique ids for the widgets
            $cat = $this->categories[$i];
            $this->Pagination( $cat['categorias'] );

            $json_data[] = [
                "id" => $cat['categorias'],
                "title" => $cat['titulo'],
                "link" => $this->category_link . $cat['categorias'] . "&ref={$i}",
                "next" => $this->category_link . $cat['categorias'] . "&ref={$i}&page=" . ( $this->current_page+1 ),
                "type" => "standard",
                "widgets" => [[
                    "id" => $this->unique_id,
                    "title" => "",
                    "type" => "newsList",
                    "items" => $this->GetCategory( $cat['categorias'] )
                ]]
            ];
            //return $json_data;
        }

        return $json_data;
    }

    /**
     * Category requested
     * @param $category
     * @return array
     */
    public function RequestCategory( $category )
    {
        $json_data = [];

        $this->menu_id = $this->GetMenuID();

        $this->Pagination( $category );
        $json_data[] = [
            "id" => (string)$category,
            "title" => get_post_meta( $this->menu_id, "widget_{$_GET['ref']}_cat-titulo",true ),
            "link" => $this->category_link . $category . "&ref={$_GET['ref']}",
            "next" => $this->category_link . $category . "&ref={$_GET['ref']}&page=" . ( $this->current_page+1 ),
            "type" => "standard",
            "widgets" => [[
                "id" => $this->unique_id,
                "title" => "",
                "type" => "newsList",
                "items" => $this->GetCategory( $category )
            ]]
        ];
        //return $json_data;

        return $json_data;
    }

    /**
     * @return array
     * @param $category_id
     */
    public function GetCategory( $category_id )
    {
        global $wpdb;

        $items = [];

        $res = $wpdb->get_results( "SELECT
									posts.ID AS id,
									posts.post_date AS date,
									posts.post_content AS content,
									posts.post_modified AS modified,
									posts.guid AS link,
									posts.post_title AS title,
									video.meta_value AS video
									FROM {$wpdb->prefix}term_relationships
									INNER JOIN {$wpdb->prefix}posts AS posts ON posts.ID = object_id
									  AND posts.post_status = 'publish'
									  AND posts.post_date <= NOW()
									LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = object_id
									  AND video.meta_key = 'mkdf_post_video_id_meta'
									WHERE term_taxonomy_id IN ({$category_id})
									ORDER BY posts.post_date DESC
									LIMIT {$this->page},{$this->limit}" );
        foreach ( $res AS $post ){
            $img = [ "source_url" => get_the_post_thumbnail_url( $post->id ) ];
            $items[] = [
                "id" => $post->id,
                "date" => str_replace(' ', "T", $post->date ),
                "modified" => str_replace(' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "image" => $img != null ? $img : null,
                "video" => $post->video != "" ? "http://videos.sapo.pt/{$post->video}" : null,
                "gallery" => $this->postformat == "gallery" ? $this->ImageGallery( $post->content ) : null,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "premiumLink" => $this->PremiumLink( $post->content ),
                "detailsLink" => $this->details_link . $post->id
                //"related" => $this->PostRelated( $post->id )
            ];

            //set the premium to false
            $this->premium = false;
        }
        return $items;
    }

    /**
     * @param $category
     * pagination
     */
    private function Pagination( $category )
    {
        //unique ids for the widgets
        if( isset($_GET['page']) ){
            $this->unique_id = "{$category}_{$_GET['page']}";
            $this->page = $_GET['page'] * $this->limit;
            $this->current_page = $_GET['page'];
        }else{
            $this->unique_id = "{$category}_0";
            $this->page = 0;
            $this->current_page = 0;
        }
    }

    /**
     * List of all categories from menu
     */
    private function ListCategories(  )
    {
        global $wpdb;

        $menu = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta
                                          WHERE post_id = {$this->menu_id} AND meta_key LIKE 'widget_%'" );
        //save all the categories on array
        for( $i = 0; $i < 10 ; $i++ ){
            $cat = get_post_meta( $this->menu_id, "widget_{$i}_cat-categorias", true );
            if( $cat != null ) {
                $this->categories[$i]['categorias'] = implode( ",", $cat);
                //$cats = implode( ",", $cat);
                foreach ($menu as $item) {
                    switch ($item->meta_key) {
                        case "widget_{$i}_cat-estado":
                            $this->categories[$i]['estado'] = $item->meta_value;
                            //$state = $item->meta_value;
                            break;
                        case "widget_{$i}_cat-quantidade":
                            $this->categories[$i]['quantidade'] = $item->meta_value;
                            //$quantity = $item->meta_value;
                            break;
                        case "widget_{$i}_cat-titulo":
                            $this->categories[$i]['titulo'] = $item->meta_value;
                            //$title = $item->meta_value;
                            break;
                        case "widget_{$i}_cat-apresentar":
                            $this->categories[$i]['apresentar'] = $item->meta_value;
                            //$show = $item->meta_value;
                            break;
                    }
                }
                /*$this->categories[] = [[
                    "categorias" => $cats,
                    "apresentar" => $show,
                    "estado"  => $state,
                    "quantidade" => $quantity,
                    "titulo" => $title
                ]];*/
            }
        }
    }

}

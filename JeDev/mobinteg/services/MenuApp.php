<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/25/19
 * Time: 3:03 PM
 */

class MenuApp extends JeTools
{

    private $unique_id, $page, $current_page;

    //
    private $postlimit = 0;

    /**
     * Loop into each widget menu
     */
    public function MenuService ()
    {
        $json_data = [];

        $this->Pagination();

        //get item

        $json_data[] = [
            "id" => "{$this->menuid}",
            "title" => "Início",
            "link" => $this->artigolink,
            "next" => $this->artigolink . "&page=" . ($this->current_page + 1),
            "type" => "standard",
            "widgets" => $this->Home()
        ];

        if ( isset( $_GET['home'] ) ) return $json_data;

        $category = new Categories();

        $data = $category->RunCategories( $this->menuid );

        foreach ($data as $data) {
            $json_data[] = $data;
        }

        return $json_data;
    }

    /**
     * Home page
     */
    private function Home ()
    {
        global $wpdb;

        $items = [];

        //how many widgets exists
        $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$this->menuid}
                                    AND meta_key LIKE 'estado%'" );

        foreach ($res as $state) {
            if ( $state->meta_value == 1 ) {
                $id = explode( "-", $state->meta_key )[1];
                $widget = $wpdb->get_results( "SELECT
                                                    apresentar.meta_value AS apresentar,
                                                    widget.meta_value AS widget_type,
                                                    quantitate.meta_value AS quantidade,
                                                    conteudo.meta_value AS conteudo,
                                                    artigo.meta_value AS artigo,
                                                    titulo.meta_value AS titulo ,
                                                    categorias.meta_value AS categorias
                                                    FROM {$wpdb->prefix}postmeta AS apresentar
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS widget ON widget.post_id = apresentar.post_id
                                                    AND widget.meta_key = 'widget-type-{$id}'
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS quantitate ON quantitate.post_id = apresentar.post_id
                                                    AND quantitate.meta_key = 'quantidade-{$id}'
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS conteudo ON conteudo.post_id = apresentar.post_id
                                                    AND conteudo.meta_key = 'conteudo-{$id}'
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS artigo ON artigo.post_id = apresentar.post_id
                                                    AND artigo.meta_key = 'artigo-{$id}'
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS titulo ON titulo.post_id = apresentar.post_id
                                                    AND titulo.meta_key = 'titulo-{$id}'
                                                    LEFT JOIN {$wpdb->prefix}postmeta AS categorias ON categorias.post_id = apresentar.post_id
                                                    AND categorias.meta_key = 'categorias-{$id}'
                                                    WHERE apresentar.post_id = {$this->menuid} AND apresentar.meta_key LIKE 'apresentar-{$id}'" );
                //run all the rules
                $item = $this->Rules( $widget, $id );
                if ( $item != null ) {
                    $items[] = $item;
                }
            }
        }

        return $items;

    }

    /**
     * @param $post_id
     * @param $cats
     * @param $limit
     * @return array
     */
    private function GetArtigo ($post_id, $cats, $limit)
    {
        global $wpdb;

        $items = [];

        if( isset($_SESSION['posts_used']) && $limit != 1 ){
            $posts = implode( ",", $_SESSION['posts_used'] );
        }else{ $posts = "9999999999999999999999999999999999999"; }

        if ( $cats != "" ) {
            $cats = implode( ",", $cats );
            $res = $wpdb->get_results( "SELECT
									posts.ID AS id,
									posts.post_date AS date,
									posts.post_content AS content,
									posts.post_modified AS modified,
									posts.guid AS link,
									posts.post_title AS title,
									video.meta_value AS video
									FROM {$wpdb->prefix}term_relationships
									INNER JOIN {$wpdb->prefix}posts AS posts ON posts.ID = object_id
									  AND posts.post_status = 'publish' AND posts.ID NOT IN ({$posts})
									  AND posts.post_date <= NOW()
									LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = object_id
									  AND video.meta_key = 'mkdf_post_video_id_meta'
									WHERE term_taxonomy_id IN ({$cats})
									ORDER BY posts.post_date DESC
									LIMIT {$limit}" );
        } else {
            $res = $wpdb->get_results( "SELECT
                                        posts.ID AS id,
                                        posts.post_date AS date,
                                        posts.post_content AS content,
                                        posts.post_modified AS modified,
                                        posts.guid AS link,
                                        posts.post_title AS title,
                                        video.meta_value AS video
                                        FROM {$wpdb->prefix}posts AS posts
                                        LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                                        AND video.meta_key = 'mkdf_post_video_id_meta'
                                        WHERE posts.ID IN ({$post_id})" );
        }

        //save the post used
        if(isset($_SESSION['posts_used']) && $limit == 1 ){
            $search = array_search( $res[0]->id, $_SESSION['posts_used'] );
            if( $search > 0 || gettype($search) != "boolean" ){

            }else{
                $_SESSION['posts_used'][] = $res[0]->id;
            }
        }else if( $limit == 1 ){
            $_SESSION['posts_used'][] = $res[0]->id;
        }

        foreach ($res as $post) {
            $img = ["source_url" => get_the_post_thumbnail_url( $post->id )];

            $items[] = [
                "id" => $post->id,
                "date" => str_replace( ' ', "T", $post->date ),
                "modified" => str_replace( ' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "video" => $post->video == "" ? null : $post->video,
                "image" => $img != null ? $img : null,
                //"gallery" => $res[0]->gallery,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "detailsLink" => $this->details_link . $post->id,
                "premiumLink" => $this->PremiumLink( $post->content ),
                "related" => $this->PostRelated( $post->id )
            ];
        }

        return $items;
    }

    /**
     * Run the rules
     * @param $widget
     * @param $id
     * @return array
     */
    private function Rules ($widget, $id)
    {
        $show = $widget[0]->apresentar;

        //if the widget only need to show after first page
        if ( $show == "two" && isset( $_GET['page'] ) && $_GET['page'] != 0 ) {
            return $this->ContentType( $widget, $id );
        } else {
            // "one" rule
            if ( $show == "one" && isset( $_GET['page'] ) && $_GET['page'] == 0 || $show == "one" && !isset( $_GET['page'] ) ) {
                return $this->ContentType( $widget, $id );
            } else if ( $show == "all" ) { // show on all pages
                return $this->ContentType( $widget, $id );
            }
        }
    }

    /**
     * @param $widget
     * @param $id
     * @return array
     */
    private function ContentType ($widget, $id)
    {
        //$data = [];
        //content type
        $type = $widget[0]->conteudo;

        //get a specific post
        if ( $type == "post" ) {
            $data = [
                "id" => $id . "_{$this->current_page}",
                "title" => $widget[0]->titulo,
                "type" => $widget[0]->widget_type,
                "items" => $this->WidgetType( $widget, $id )
            ];
        } else {//get the last post from a category
            $data = [
                "id" => $id . "_{$this->current_page}",
                "title" => $widget[0]->titulo,
                "type" => $widget[0]->widget_type,
                "items" => $this->WidgetType( $widget, $id )
            ];
        }

        return $data;
    }

    /**
     * @param $id
     * @param $widget
     * @return array
     */
    private function WidgetType ($widget, $id)
    {
        $type = $widget[0]->widget_type;

        if ( $type == "headline" ) {//post choice
            if ( $widget[0]->conteudo == "post" ) {
                $post = unserialize($widget[0]->artigo);
                $items = $this->GetArtigo( $post[0], "", 1 );
            } else {//last of categories
                $categories = unserialize($widget[0]->categorias);
                $items = $this->GetArtigo( "", $categories, 1 );
            }
            return $items;
        } else if ( $type == "newsList" ) {
            $cat_ids = unserialize($widget[0]->categorias);
            if ( $cat_ids == null ) return null;

            $cat_ids = implode( ",", $cat_ids );

            return $this->NewsList( $cat_ids, $widget[0]->quantidade );
        } else if ( $type == "highlight" || $type == "featuredDark" || $type == "featuredLight" ) {
            if ( $widget[0]->conteudo == "post" ) {
                //if he select the posts
                $posts_ids = unserialize($widget[0]->artigo);
                if ( $posts_ids == null ) return null;
                $posts_ids = implode( ",", $posts_ids );
                return $this->GetArtigos( $posts_ids );
            } else {
                //if he select categories
                $categories = unserialize($widget[0]->categorias);
                return $this->GetArtigo( "", $categories, $widget[0]->quantidade );
            }
        } else if ( $type == "authors" || $type == "authorsList" ) {
            if ( $widget[0]->conteudo == "post" ) {
                //get the select author opinions
                return $this->GetAuthors( $id );
            } else {
                // get the last authors opinions
                return $this->GetArtigo( "", [17], $widget[0]->quantidade );
            }
        } else if ( $type == "ad" ) {
            //call the ad
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function GetAuthors ($id)
    {
        //posts ids
        $posts_ids = get_post_meta( $this->menuid, "artigo-{$id}-author", true );
        $posts_ids = implode( ",", $posts_ids );
        global $wpdb;

        $posts = [];

        $res = $wpdb->get_results( "SELECT
                                        posts.ID AS id,
                                        posts.post_date AS date,
                                        posts.post_content AS content,
                                        posts.post_modified AS modified,
                                        posts.guid AS link,
                                        posts.post_title AS title,
                                        video.meta_value AS video
                                        FROM {$wpdb->prefix}posts AS posts
                                        LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                                        AND video.meta_key = 'mkdf_post_video_id_meta'
                                        WHERE posts.ID IN ({$posts_ids})" );

        //return $res[0]->id;

        foreach ($res as $post) {
            $img = ["source_url" => get_the_post_thumbnail_url( $post->id )];
            $posts[] = [
                "id" => $post->id,
                "date" => str_replace( ' ', "T", $post->date ),
                "modified" => str_replace( ' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "video" => $post->video == "" ? null : $post->video,
                "image" => $img != null ? $img : null,
                //"gallery" => $post->gallery,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "premiumLink" => $this->PremiumLink( $post->content ),
                "detailsLink" => $this->details_link . $post->id,
                //"related" => $this->PostRelated( $post->id )
            ];
        }
        return $posts;
    }

    /**
     * @param $cats
     * @param $quantity
     * @return array
     */
    private function NewsList ($cats, $quantity)
    {
        global $wpdb;

        $posts = [];
        if ( isset( $_GET['page'] ) ) {
            $limit = $quantity * $_GET['page'];
        } else {
            $limit = $this->postlimit;
        }

        if( isset($_SESSION['posts_used']) ) {
            $posts_ = implode( ",", $_SESSION['posts_used'] );
        }else{//default value
            $posts_ = "99999999999999999999999999999";
        }

        $res = $wpdb->get_results( "SELECT
                                                posts.ID AS id,
                                                posts.post_date AS date,
                                                posts.post_content AS content,
                                                posts.post_modified AS modified,
                                                posts.guid AS link,
                                                posts.post_title AS title,
                                                video.meta_value AS video
                                            FROM
                                                {$wpdb->prefix}term_relationships
                                                    INNER JOIN
                                                {$wpdb->prefix}posts AS posts ON posts.ID = object_id
                                                    AND posts.post_status = 'publish'
                                                    AND posts.post_date <= NOW()
                                                    AND posts.ID NOT IN ({$posts_})
                                                    LEFT JOIN
                                                {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                                                    AND video.meta_key = 'mkdf_post_video_id_meta'
                                            WHERE
                                                term_taxonomy_id IN ({$cats})
                                            GROUP BY id
                                            ORDER BY date DESC
                                            LIMIT {$limit},{$quantity}" );

        //return $res[0]->id;

        foreach ($res as $post) {
            $img = ["source_url" => get_the_post_thumbnail_url( $post->id )];
            $posts[] = [
                "id" => $post->id,
                "date" => str_replace( ' ', "T", $post->date ),
                "modified" => str_replace( ' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "video" => $post->video == "" ? null : $post->video,
                "image" => $img != null ? $img : null,
                //"gallery" => $post->gallery,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "premiumLink" => $this->PremiumLink( $post->content ),
                "detailsLink" => $this->details_link . $post->id,
                //"related" => $this->PostRelated( $post->id )
            ];
            $this->premium = false;
        }
        $this->postlimit += $quantity;
        return $posts;
    }

    /**
     *
     */
    private function Pagination ()
    {
        //unique ids for the widgets
        if ( isset( $_GET['page'] ) ) {
            //$this->unique_id = "{$category}_{$_GET['page']}";
            //$this->page = $_GET['page'] * $this->limit;
            $this->current_page = $_GET['page'];
        } else {
            //$this->unique_id = "{$category}_0";
            //$this->page = 0;
            $this->current_page = 0;
        }
    }


    private function PostsUsed ()
    {
        global $wpdb;


    }

}

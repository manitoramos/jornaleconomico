<?php

if ( !defined( 'ABSPATH' ) ) {
    require_once __DIR__ . '/../../../wp-load.php';
}
global $wpdb;
//global funcs
require_once __DIR__ . '/tools/GlobalFuncs.php';

require_once __DIR__ . '/services/Category.php';
require_once __DIR__ . '/services/MenuApp.php';

$menu = new MenuApp();
$feed = $menu->MenuService();
$feed = json_encode($feed);

$myfile = fopen(__DIR__ . "/cache/home.txt", "w") or die("Unable to open file!");
fwrite($myfile, $feed);
fclose($myfile);

$feed = addslashes( $feed );
$date = date("Y-m-d H:i:s");
$wpdb->query( "INSERT INTO {$wpdb->prefix}cron_logs (log,date_run) VALUES ('{$feed}','{$date}')" );

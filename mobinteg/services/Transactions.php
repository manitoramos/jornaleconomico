<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 3/14/19
 * Time: 3:45 PM
 */

class Transactions
{

    public function Insert ()
    {
        global $wpdb;

        //check the parameters and if not empty throw the error given
        $status = $this->CheckParameters();
        if ( !empty( $status ) ) {
            return $this->ThrowError( $status );
        }

        $hashname = substr( md5( openssl_random_pseudo_bytes( 20 ) ), -32 );
        $num = rand( 1, 1000000000000 );

        $random_name = $hashname . $num;

        //return $random_name;

        //vars
        $user_id = isset( $_POST['user_id'] ) ? $_POST['user_id'] : "";;
        $email = isset( $_POST['email'] ) ? $_POST['email'] : "";
        $source = $_POST['source'];
        $product_id = $_POST['product_id'];
        $receipt_id = $_POST['receipt_id'];
        $transaction_id = $_POST['transaction_id'];
        $purchase_date = $_POST['purchase_date'];
        $rawdata = $_POST['rawdata'];
        $duration = $_POST['duration'];
        $expiration_date = $_POST['expiration_date'];

        $post_id = wp_insert_post(
            [
                "post_title" => $transaction_id,
                "post_type" => "transaction",
                "post_status" => "publish"
            ]
        );

        //user id
        update_post_meta( $post_id, "email", $email );
        update_post_meta( $post_id, "product-id", $product_id );
        update_post_meta( $post_id, "receipt-id", $receipt_id );
        update_post_meta( $post_id, "transaction-id", $transaction_id );
        update_post_meta( $post_id, "source", $source );
        update_post_meta( $post_id, "purchase-date", $purchase_date );
        update_post_meta( $post_id, "duration", $duration );// 7 days or 1 year
        update_post_meta( $post_id, "expiration-date", $expiration_date );

        $table_name = $wpdb->prefix . 'mobile_transactions';
        $wpdb->insert(
            $table_name,
            array(
                'userID' => $user_id,
                'email' => $email,
                'productID' => $product_id,
                'source' => $source,
                'purchaseDate' => $purchase_date,
                'expirationDate' => $expiration_date,
                'transactionID' => $transaction_id,
                'receiptID' => $receipt_id,
                'rawData' => $rawdata,
                'duration' => $duration),
            array('%d', '%s', '%d', '%s', '%s', '%s', '%d', '%d', '%s', '%s')
        );

        if ( $wpdb->last_error !== '' ) return $this->ThrowError( $wpdb->print_error() );

        return [
            "success"
        ];

    }

    public function Get ()
    {

    }

    /**
     * @return string
     */
    public function CheckParameters ()
    {

        if ( !isset( $_POST['source'] ) ) {
            return "source required";
        } else if ( !isset( $_POST['product_id'] ) ) {
            return "product_id required";
        } else if ( !isset( $_POST['receipt_id'] ) ) {
            return "receipt_id required";
        } else if ( !isset( $_POST['transaction_id'] ) ) {
            return "transaction_id required";
        } else if ( !isset( $_POST['purchase_date'] ) ) {
            return "purchase_date required";
        } else if ( !isset( $_POST['expiration_date'] ) ) {
            return "expiration_date required";
        } else if ( !isset( $_POST['duration'] ) ) {
            return "duration required";
        } else if ( !isset( $_POST['rawdata'] ) ) {
            return "rawdata required";
        }

    }

    /**
     * @param $error
     * @return array
     */
    public function ThrowError ($error)
    {
        return [
            "error" => [
                "msg" => $error
            ]
        ];
    }

}

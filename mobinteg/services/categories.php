<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/20/19
 * Time: 2:10 PM
 */


Class Category extends JeTools
{

    private $categories = [
        10 => "Economia",
        11 => "Politica",
        12 => "Empresas",
        16 => "Mercados",
        10748 => "Advisory",
        14 => "Mundo",
        16891 => "Je-Tv",
        17 => "Opinião",
        15 => "Cultura-Lifestyle"
    ];

    //default limit
    private $limit = 20;

    //pagination is the page number * limit default (20)
    private $page;

    //current page
    private $current_page;

    //unique id for the widget
    private $unique_id;

    //url for widgets options
    private $widgets = "https://dev.megafin.pt/wp-json/acf/v3/options/application-settings";

    /**
     * Loop into each category
     */
    public function RunCategories()
    {

        $json_data = [];

        foreach ( $this->categories AS $category => $name ){
            //unique ids for the widgets
            $this->Pagination( $category );

            $json_data[] = [
                "id" => (string)$category,
                "title" => $name,
                "link" => $this->category_link . $category,
                "next" => $this->category_link . $category . "&page=" . ( $this->current_page+1 ),
                "type" => "standard",
                "widgets" => [[
                    "id" => $this->unique_id,
                    "title" => "",
                    "type" => "newsList",
                    "items" => $this->GetCategory( $category )
                ]]
            ];
            //return $json_data;
        }
        return $json_data;
    }

    /**
     * Category requested
     * @param $category
     * @return array
     */
    public function RequestCategory( $category )
    {
        $json_data = [];

            $this->Pagination( $category );

            $json_data[] = [
                "id" => (string)$category,
                "title" => $this->categories[ $category ],
                "link" => $this->category_link . $category,
                "next" => $this->category_link . $category . "&page=" . ( $this->current_page+1 ),
                "type" => "standard",
                "widgets" => [[
                    "id" => $this->unique_id,
                    "title" => "",
                    "type" => "newsList",
                    "items" => $this->GetCategory( $category )
                ]]
            ];
            //return $json_data;

        return $json_data;
    }

    /**
     * @return array
     * @param $category_id
     */
    public function GetCategory( $category_id )
    {
        global $wpdb;

        $items = [];

        $res = $wpdb->get_results( "SELECT
									posts.ID AS id,
									posts.post_date AS date,
									posts.post_content AS content,
									posts.post_modified AS modified,
									posts.guid AS link,
									posts.post_title AS title,
									video.meta_value AS video
									FROM {$wpdb->prefix}term_relationships
									INNER JOIN {$wpdb->prefix}posts AS posts ON posts.ID = object_id
									LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = object_id
									  AND video.meta_key = 'mkdf_post_video_id_meta'
									WHERE term_taxonomy_id = {$category_id}
									ORDER BY posts.post_date DESC
									LIMIT {$this->page},{$this->limit}" );
        foreach ( $res AS $post ){
            $img = [ "source_url" => get_the_post_thumbnail_url( $post->id ) ];
            $items[] = [
                "id" => $post->id,
                "date" => str_replace(' ', "T", $post->date ),
                "modified" => str_replace(' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "image" => $img != null ? $img : null,
                "video" => $post->video != "" ? "http://videos.sapo.pt/{$post->video}" : null,
                "gallery" => $this->postformat == "gallery" ? $this->ImageGallery( $post->content ) : null,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "detailsLink" => $this->details_link . $post->id
                //"related" => $this->PostRelated( $post->id )
            ];

            //set the premium to false
            $this->premium = false;
        }
        return $items;
    }

    /**
     * @param $category
     * pagination
     */
    private function Pagination( $category )
    {
        //unique ids for the widgets
        if( isset($_GET['page']) ){
            $this->unique_id = "{$category}_{$_GET['page']}";
            $this->page = $_GET['page'] * $this->limit;
            $this->current_page = $_GET['page'];
        }else{
            $this->unique_id = "{$category}_0";
            $this->page = 0;
            $this->current_page = 0;
        }
    }

}

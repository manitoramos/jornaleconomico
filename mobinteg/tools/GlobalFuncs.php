<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/21/19
 * Time: 3:30 PM
 */

class JeTools
{

    public $format = [
        10792, //video
        12003, //gallery
        14775  //citação
    ];

    public $format_names = [
        10792 => "video",
        12003 => "gallery",
        14775 => "standard"//citação
    ];

    public $postformat;
    //to know if post is premium or not
    public $premium = false;

    public $details_link;

    public $category_link;

    public $artigolink;

    public $menuid;

    public $menu_name = "app-settings";

    function __construct ()
    {
        $this->details_link = get_site_url() . "/wp-json/je/v1/feed?details=";

        $this->category_link = get_site_url() . "/wp-json/je/v1/feed?cat=";

        $this->artigolink = get_site_url() . "/wp-json/je/v1/feed?home=true";
        //get the menu id
        $this->menuid = $this->GetMenuID();

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * @param $post_id
     * @return mixed|string
     */
    public function PostFormat( $post_id )
    {
        global $wpdb;

        $format_ids = implode( ",", $this->format );

        $res = $wpdb->get_results( "SELECT * FROM wp_term_relationships
                                          WHERE object_id = {$post_id} AND term_taxonomy_id IN ({$format_ids})" );

        if( sizeof($res) > 0 ){
            $this->postformat = isset( $this->format_names[$res[0]->term_taxonomy_id] ) ? $this->format_names[$res[0]->term_taxonomy_id] : "standard";
            return $this->postformat;
        }

        return "standard";

    }

    /**
     * @param $content
     * for format gallery
     * @return false|mixed|string|void
     */
    public function ImageGallery( $content )
    {
        $images = [];
        //get the images ids
        try {
            $img_ids = explode( 'ids="', $content ) ;
            if( isset($img_ids[1]) && $img_ids[1] != ""){
                $img_ids = $img_ids[1];
            }else{
                return null;
            }

            $img_ids = explode( '"', $img_ids )[0];
            $img_ids = explode( ",", $img_ids );

            foreach ($img_ids as $id) {
                $images[] = wp_get_attachment_url( $id );
            }

            return $images;
        }catch (Exception $e){
            return null;
        }
    }

    /**
     * @param $post_id
     * @return array
     */
    public function Authors($post_id)
    {
        $authors = [];
        global $wpdb;

        $res = $wpdb->get_results( "SELECT terms.name,taxo.description FROM `wp_term_relationships` as relation
                                  inner join wp_term_taxonomy as taxo on taxo.term_id = relation.term_taxonomy_id
                                  inner join wp_terms as terms on terms.term_id = relation.term_taxonomy_id
                                  and taxo.taxonomy = 'author'
                                  WHERE relation.object_id = {$post_id}" );
        foreach ( $res as $author ){
            $user = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}users WHERE user_login = '{$author->name}'" );
            if( $user ){
                $authors[] = [
                    "id" => $user[0]->ID,
                    "name" => $user[0]->display_name
                ];
            }else{
                $id = preg_replace('/\D/', '', $author->description);
                $name = get_post_meta( $id, "cap-display_name", true );
                $authors[] = [
                    "id" => $id,
                    "name" => $name
                ];
            }

        }

        return $authors;

    }

    /**
     * @param $post_id
     * Return the categories and check if the post is premium
     * @return array
     */
    public function PostCategories($post_id)
    {
        $categories = [];
        global $wpdb;

        $res = $wpdb->get_results( "SELECT terms.name, terms.term_id FROM `wp_term_relationships` as relation
                                          inner join wp_term_taxonomy as taxo on taxo.term_id = relation.term_taxonomy_id
                                          inner join wp_terms as terms on terms.term_id = relation.term_taxonomy_id
                                          and taxo.taxonomy = 'category'
                                          WHERE relation.object_id = {$post_id}" );

        foreach ( $res as $category ){
            //premium content as this id
            if( $category->term_id == 12361 ) $this->premium = true;

            $categories[] =[
                "id" => $category->term_id,
                "name" => $category->name
            ];
        }

        return $categories;
    }

    /**
     * @param $post_id
     * @return array|null
     */
    public function PostRelated($post_id)
    {
        $posts = [];

        global $wpdb;

        $post = get_post_meta( $post_id, "related_posts", true );

        if ( !empty( $post ) ){
            $post = implode( ",", $post );

            //$res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE ID IN ({$post})" );

            $res = $wpdb->get_results( "SELECT
                                        posts.ID AS id,
                                        posts.post_date AS date,
                                        posts.post_content AS content,
                                        posts.post_modified AS modified,
                                        posts.post_excerpt AS excerpt,
                                        posts.guid AS link,
                                        posts.post_title AS title,
                                        video.meta_value AS video
                                        FROM wp_posts as posts
                                        LEFT JOIN wp_postmeta AS video ON video.post_id = posts.ID
                                        AND video.meta_key = 'mkdf_post_video_id_meta'
                                        WHERE posts.ID IN ({$post})" );

            foreach ( $res as $post ){
                $img = [ "source_url" => get_the_post_thumbnail_url( $post->id ) ];
                $posts[] = [
                    "id" => $post->id,
                    "date" => str_replace(' ', "T", $post->date ),
                    "modified" => str_replace(' ', "T", $post->modified ),
                    "link" => $post->link,
                    "title" => $post->title,
                    "format" => $this->PostFormat( $post->id ),
                    "image" => $img != null ? $img : null,
                    "video" => $post->video != "" ? "http://videos.sapo.pt/{$post->video}" : null,
                    "gallery" => $this->postformat == "gallery" ? $this->ImageGallery( $post->content ) : null,
                    "authors" => $this->Authors( $post->id ),
                    "categories" => $this->PostCategories( $post->id ),
                    "premium" => $this->premium,
                    "detailsLink" => $this->details_link . $post->id
                ];
            }

            return $posts;
        }

        return null;
    }

    /**
     * Errors
     * @param $error
     * @return array
     */
    public function Error( $error )
    {
        return [
          "status" => "error",
          "msg" => $error
        ];
    }

    public function PremiumLink( $content )
    {
        if( $this->premium == true ) {
            $link = $content;
            preg_match_all( '/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $link, $result );

            if ( !empty( $result ) ) {
                # Found a link.
                if( isset( $result['href'][sizeof( $result['href'] ) - 1] ) ) return $result['href'][sizeof( $result['href'] ) - 1];
            }
        }
    }

    public function GetMenuID()
    {
        global $wpdb;
        $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE post_name = '{$this->menu_name}'" );
        return $res[0]->ID;
    }

    public function GetArtigos ($post_id)
    {
        global $wpdb;

        $items = [];

        $res = $wpdb->get_results( "SELECT
                                        posts.ID AS id,
                                        posts.post_date AS date,
                                        posts.post_content AS content,
                                        posts.post_modified AS modified,
                                        posts.guid AS link,
                                        posts.post_title AS title,
                                        video.meta_value AS video
                                        FROM {$wpdb->prefix}posts AS posts
                                        LEFT JOIN {$wpdb->prefix}postmeta AS video ON video.post_id = posts.ID
                                        AND video.meta_key = 'mkdf_post_video_id_meta'
                                        WHERE posts.ID IN ({$post_id})" );


        foreach ($res as $post) {
            $img = ["source_url" => get_the_post_thumbnail_url( $post->id )];

            $items[] = [
                "id" => $post->id,
                "date" => str_replace(' ', "T", $post->date ),
                "modified" => str_replace(' ', "T", $post->modified ),
                "link" => $post->link,
                "title" => $post->title,
                "format" => $this->PostFormat( $post->id ),
                "video" => $post->video == "" ? null : $post->video,
                "image" => $img != null ? $img : null,
                //"gallery" => $res[0]->gallery,
                "authors" => $this->Authors( $post->id ),
                "categories" => $this->PostCategories( $post->id ),
                "premium" => $this->premium,
                "detailsLink" => $this->details_link . $post->id,
                "premiumLink" => $this->PremiumLink( $post->content ),
            ];
        }

        return $items;
    }
}
